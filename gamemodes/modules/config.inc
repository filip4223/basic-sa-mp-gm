#define SCRIPT_TAG 		"TAG"
#define SCRIPT_VERSION	"1.0"
#define SCRIPT_NAME		"Script name"

//Main MySQL handler
new MySQL:m_sql;

//Enumerator's
enum eSettings
{
	setting_weather,
	setting_time,
	bool:setting_show_nametags,
	setting_markers,
	Float:setting_nametags_drawdistance,
	bool:setting_stunts,
	bool:setting_default_interiors,
}
new Setting[eSettings];