# Basic SA-MP GM

It's only base of SA:MP gamemode intended for beginners or people, who dont
know how to start with scripting in SAMP. 

Version 1.1:
- Support for multilingualism; add enumerators and created basic structure 
for players

Version 1.0:
- Initial commit;
- Almost empty script, only with MySQL connect (BlueG R41-4)